/* MAC 425-5739 - Inteligência Artificial
 * Segundo Exercício-Programa (EP2)
 * Arthur de Moura Del Esposte
 * Aluno Especial do Programa de Pós Gradução em Ciência da Computação do IME
 *  */

:- dynamic yes/1,no/1.

go :- nl,
      write('Pense em um personagem da Marvel, e eu tentarei adivinhar quem é. Pensou (sim/não)?'),
      nl,
      read(Response),
      nl,
      (Response == sim ; Response == s),
      write('Ótimo! Agora, responda sim ou não às seguintes perguntas:'),
      nl, nl,
      guess(Personagem),
      write('O personagem é '),
      write(Personagem),
      nl,
      undo.

/* hipóteses a serem testadas */
guess(magneto)   :- magneto, !.
guess(loki) :- loki, !.
guess(encantor) :- encantor, !.
guess(caveira_vermelha)   :- caveira_vermelha, !.
guess(duende_verde) :- duende_verde, !.
guess(ciclope) :- ciclope, !.
guess(tempestade) :- tempestade, !.
guess(demolidor) :- demolidor, !.
guess(capitão_américa) :- capitão_américa, !.
guess(viúva_negra) :- viúva_negra, !.
guess(thor) :- thor, !.
guess(odin) :- odin, !.
guess(lady_sif) :- lady_sif, !.
guess(desconhecido).

/* regras */

herói:- verify(é_herói),!.
vilão:- not(herói),!.

humano:- ( herói ; vilão),
    verify(é_humano),!.

asgardiano:- ( herói ; vilão),
    not(humano),!.

vingador:- ( humano ; asgardiano),
    verify(é_membro_dos_vingadores),!.

xmen:- humano,
    not(vingador),
    verify(é_membro_dos_xmen),!.

mutante:- humano,
    verify(é_mutante),!.

hydra:- humano,
    verify(é_membro_da_hydra),!.

magneto :- humano,
    vilão,
    mutante,!.

caveira_vermelha :- humano,
    vilão,
    not(mutante),
    hydra,!.

duende_verde :- humano,
    vilão,
    not(mutante),
    not(hydra),!.

loki :- asgardiano,
    vilão,
    verify(é_do_gênero_masculino),!.

encantor :- asgardiano,
    vilão,
    verify(é_do_gênero_feminino),!.

ciclope :- humano,
    herói,
    xmen,
    verify(é_do_gênero_masculino),!.

tempestade :- humano,
    herói,
    xmen,
    verify(é_do_gênero_feminino),!.

demolidor :- humano,
    herói,
    not(vingador),
    not(xmen),!.

capitão_américa :- humano,
    herói,
    vingador,
    verify(é_do_gênero_masculino),!.

viúva_negra :- humano,
    herói,
    vingador,
    verify(é_do_gênero_feminino),!.

thor :- asgardiano,
    herói,
    vingador,!.

odin :- asgardiano,
    herói,
    not(vingador),
    verify(é_do_gênero_masculino),!.

lady_sif :- asgardiano,
    herói,
    not(vingador),
    verify(é_do_gênero_feminino),!.


tem_gênero:- verify(é_do_gênero_masculino); verify(é_do_gênero_feminino),!.
no(é_do_gênero_feminino):- yes(é_do_gênero_masculino).
yes(é_do_gênero_feminino):- no(é_do_gênero_masculino).


/* Selecionador de perguntas */
ask(Question) :-
    write('O personagem  '),
    write(Question),
    write('? '),
    read(Response),
    nl,
    ( (Response == sim ; Response == s)
      ->
      assert(yes(Question)) ;
       assert(no(Question)), fail).


/* Verificador de respostas */
verify(S) :-
    (yes(S) -> true ; (no(S) -> fail ; ask(S))).

/* desfaz asserções */
undo :- retract(yes(_)),fail.
undo :- retract(no(_)),fail.
undo.

